//
//  AppDelegate.swift
//  CoordinatorPOC
//
//  Created by Daniel Esteban Cardona Rojas on 10/10/18.
//  Copyright © 2018 Daniel Esteban Cardona Rojas. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var coordinator: MainCoordinator?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        coordinator = MainCoordinator(window: UIWindow(frame: UIScreen.main.bounds))
        coordinator?.start()
        return true
    }

}

