//
//  MainCoordinator.swift
//  CoordinatorPOC
//
//  Created by Daniel Esteban Cardona Rojas on 10/10/18.
//  Copyright © 2018 Daniel Esteban Cardona Rojas. All rights reserved.
//

import UIKit

class MainCoordinator: Coordinator {
    // MARK: Coordinator
    lazy var childCoordinators: [Coordinator] = {
        let scene2Coordinator = Scene2Coordinator(navigationController: self.navigationController)
        return [scene2Coordinator]
    }()
    
    lazy var navigationController: UINavigationController = {
       return UINavigationController()
    }()
    
    lazy var initialViewController: UIViewController = {
        return Scene1FirstViewController.instantiate()!
    }()
    
    func start() {
        guard let vc = initialViewController as? Scene1FirstViewController else {
            return
        }
        
        vc.coordinator = self
        push(vc, animated: true)
    }
    
    // MARK: Initializers
    var window: UIWindow
    init(window: UIWindow) {
       self.window = window
       self.window.rootViewController = self.navigationController
       self.window.makeKeyAndVisible()
    }
    
    // MARK: Navigation
    func goToSecondViewController() {
        guard let vc = Scene1SecondViewController.instantiate() else {
            return
        }
        
        vc.coordinator = self
        push(vc, animated: true)
    }
    
    func backToFirstViewController() {
        pop(animated: true)
    }
    
    func goToScene2() {
        let scene2Coordinator = childCoordinator(ofType: Scene2Coordinator.self)
        scene2Coordinator?.delegate = self
        scene2Coordinator?.navigationController = navigationController
        scene2Coordinator?.start()
    }
    
    func showModal() {
        let alert = UIAlertController(title: "Alert", message: "Modal controller", preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alert.addAction(cancelAction)
        navigationController.topViewController?.present(alert, animated: true, completion: nil)
        
    }
}

extension MainCoordinator: Scene2CoordinatorDelegate {
    func didCompleteFlow() {
        self.removeChildCoordinator(ofType: Scene2Coordinator.self)
        self.navigationController.popToViewController(initialViewController, animated: true)
    }
}
