//
//  Scene2Coordinator.swift
//  CoordinatorPOC
//
//  Created by Daniel Esteban Cardona Rojas on 10/10/18.
//  Copyright © 2018 Daniel Esteban Cardona Rojas. All rights reserved.
//

import UIKit

protocol Scene2CoordinatorDelegate: class {
    func didCompleteFlow()
}

class Scene2Coordinator: Coordinator {
    weak var delegate: Scene2CoordinatorDelegate?
    
    // MARK: Coordinator
    var childCoordinators: [Coordinator] = []
    
    var navigationController: UINavigationController
    
    lazy var initialViewController: UIViewController = {
        return Scene2FirstViewController.instantiate()!
    }()
    
    func start() {
        guard let vc = initialViewController as? Scene2FirstViewController else {
            return
        }
        vc.coordinator = self
        push(vc, animated: true)
    }
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    // MARK: Navigation
    func goToSecondViewController() {
        guard let vc = Scene2SecondViewController.instantiate() else {
            return
        }
        
        vc.coordinator = self
        push(vc, animated: true)
    }
    
    func backToFirstScene() {
        delegate?.didCompleteFlow()
    }
    
    deinit {
        print("Deallocated Scene2coordinator")
    }

}
