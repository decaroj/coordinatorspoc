//
//  Coordinator.swift
//  CoordinatorPOC
//
//  Created by Daniel Esteban Cardona Rojas on 10/10/18.
//  Copyright © 2018 Daniel Esteban Cardona Rojas. All rights reserved.
//

/*
 
 Some facts:
 
 - Communication among Coordinators happens through delegation (if a child coordinator
 needs to talk to a parent coordinator delegation is used).
 - Coordinators will de delegates of the viewcontrollers they present.
 - Coordinators build up a tree like structure.
 - Coordinators will inject dependencies to viewcontrollers if any.
 - Coordinators will manage as many viewcontrollers as an author decides.
 
 
 Rules:
 - Coordinators should not be subclassed for removing to work correctly.

 */

import UIKit
import Foundation

protocol Coordinator: class {
    var childCoordinators: [Coordinator] { get set }
    var navigationController: UINavigationController { get set }
    var initialViewController: UIViewController { get }
    
    func start()
}

extension Coordinator {
    func push(_ viewController: UIViewController, animated: Bool) {
        viewController.onViewDidAppear {
            let vcType = String(describing: type(of: viewController))
            let presentingCoordinator = String(describing: Self.self)
            print("[\(presentingCoordinator)] \(vcType) -> viewDidAppear")
        }
        
        viewController.onViewDidDisappear {
            let vcType = String(describing: type(of: viewController))
            let presentingCoordinator = String(describing: Self.self)
            print("[\(presentingCoordinator)] \(vcType) -> viewDidDisappear")
        }
        
        navigationController.pushViewController(viewController, animated: animated)
    }
    
    func pop(animated: Bool) {
        navigationController.popViewController(animated: animated)
    }
    
    func childCoordinator<T>(ofType: T.Type) -> T? {
        let coord =  self.childCoordinators.first { (coord) -> Bool in
            let isOfCorrectType = coord is T
            return isOfCorrectType
        }
        
        return coord as? T
        
    }
    
    func removeChildCoordinator<T>(ofType: T.Type) {
        let filtered = self.childCoordinators.filter({ !($0 is T) })
        self.childCoordinators = filtered
    }
    
    func popToFirst(animated: Bool) {
       navigationController.popToViewController(initialViewController, animated: animated)
    }
    
}

protocol Coordinated: class {
    var coordinator: Coordinator? { get set }
}
