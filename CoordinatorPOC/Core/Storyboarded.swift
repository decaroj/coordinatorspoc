//
//  Storyboarded.swift
//
//  Created by Daniel Esteban Cardona Rojas on 9/5/18.
//

import UIKit

/*
 It is encouraged to use the same class name for the controller story board identifier,
 but this protocol is flexible to allow defining the story from where to instantiate the
 controller as well its identifier.
 
 
 You can choose to keep the convention while just changing the story board where the controller
 is located.
 e.g:
 
 extension SettingsViewController {
 static var board : String {
 return "SomeOtherStoryBoard"
 }
 }
 
 or implement both.
 */

protocol Storyboarded: class {
    static var board: String { get }
    static var boardId: String? { get }
    static func instantiate() -> Self?
}

//MARK: - Free implementations computed from protocol requirements
extension Storyboarded {
    
    // Assuming the initialViewController of the board is your desired controller
    static var storyboard: UIStoryboard {
        return UIStoryboard(name: Self.board, bundle: Bundle.main)
    }
    
    static func instantiateInitial() -> Self? {
        return Self.storyboard.instantiateInitialViewController() as? Self
    }
    
    // Unwrapped/Unsafe versions (will fail at runtime)
    static func instantiated() -> Self {
        return Self.instantiate()!
    }
    
    static func instantiatedInitial() -> Self {
        return Self.instantiateInitial()!
    }
    
}

// MARK: - Default implementation for UIViewController and subclasses
extension Storyboarded where Self: UIViewController {
    
    // Assumes your controller is in Main.storyboard
    static var board: String {
        return "Main"
    }
    
    static var boardId: String? {
        return nil
    }
    
    
    // Asumes your storyboard id has the same name as the viewcontroller class
    static func instantiate() -> Self? {
        let className = Self.boardId ?? String(describing: Self.self)
        return Self.storyboard.instantiateViewController(withIdentifier: className) as? Self
    }
    
    
}
