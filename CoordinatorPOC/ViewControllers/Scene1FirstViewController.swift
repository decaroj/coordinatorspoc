//
//  FirstViewController.swift
//  CoordinatorPOC
//
//  Created by Daniel Esteban Cardona Rojas on 10/10/18.
//  Copyright © 2018 Daniel Esteban Cardona Rojas. All rights reserved.
//

import UIKit

class Scene1FirstViewController: UIViewController {


    weak var coordinator: MainCoordinator?

    @IBAction func goToSecondViewController(_ sender: Any) {
        coordinator?.goToSecondViewController()
    }
    
    @IBAction func goToScene2(_ sender: Any) {
        coordinator?.goToScene2()
    }
    
    @IBAction func showAlert(_ sender: Any) {
        coordinator?.showModal()
    }

}

extension Scene1FirstViewController: Storyboarded {
    static var board: String {
        return "Scene1"
    }
}
