//
//  SecondViewController.swift
//  CoordinatorPOC
//
//  Created by Daniel Esteban Cardona Rojas on 10/10/18.
//  Copyright © 2018 Daniel Esteban Cardona Rojas. All rights reserved.
//

import UIKit

class Scene1SecondViewController: UIViewController {

    weak var coordinator: MainCoordinator?
    
    @IBAction func goToFirstViewController(_ sender: Any) {
        coordinator?.backToFirstViewController()
    }
}

extension Scene1SecondViewController: Storyboarded {
    static var board: String {
        return "Scene1"
    }
}

