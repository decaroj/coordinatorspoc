//
//  Scene2FirstViewController.swift
//  CoordinatorPOC
//
//  Created by Daniel Esteban Cardona Rojas on 10/10/18.
//  Copyright © 2018 Daniel Esteban Cardona Rojas. All rights reserved.
//

import UIKit

class Scene2FirstViewController: UIViewController {

    weak var coordinator: Scene2Coordinator?

    @IBAction func nextTapped() {
       coordinator?.goToSecondViewController()
    }

}

extension Scene2FirstViewController: Storyboarded {
    static var board: String {
        return "Scene2"
    }
}
