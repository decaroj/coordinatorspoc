//
//  Scene2SecondViewController.swift
//  CoordinatorPOC
//
//  Created by Daniel Esteban Cardona Rojas on 10/10/18.
//  Copyright © 2018 Daniel Esteban Cardona Rojas. All rights reserved.
//

import UIKit

class Scene2SecondViewController: UIViewController {

    weak var coordinator: Scene2Coordinator?
    
    @IBAction func doneTapped(_ sender: Any) {
        coordinator?.backToFirstScene()
    }
}

extension Scene2SecondViewController: Storyboarded {
    static var board: String {
        return "Scene2"
    }
}
