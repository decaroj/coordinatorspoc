//
//  CoordinatorPOCTests.swift
//  CoordinatorPOCTests
//
//  Created by Daniel Esteban Cardona Rojas on 10/10/18.
//  Copyright © 2018 Daniel Esteban Cardona Rojas. All rights reserved.
//

import XCTest
@testable import CoordinatorPOC

class CoordinatorPOCTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testChildCoordinatorCanRemoveChildCoordinator() {
        let coordinator = MockCoordinator()
        coordinator.childCoordinators = [MockCoordinator()]
        coordinator.removeChildCoordinator(ofType: MockCoordinator.self)
        XCTAssert(coordinator.childCoordinators.count == 0)
    }
    
    func testChildCoordinatorGetsDeallocatedOnRemoval() {
        let expectation = XCTestExpectation(description: "Deallocates child coordinator")
        let coordinator = MockCoordinator()
        coordinator.childCoordinators = [MockCoordinator(deallocationCallback: { expectation.fulfill() })]
        coordinator.removeChildCoordinator(ofType: MockCoordinator.self)
        wait(for: [expectation], timeout: 5.0)
    }
    

}

class MockCoordinator: Coordinator {
    var childCoordinators: [Coordinator] = []
    var navigationController: UINavigationController = UINavigationController()
    var initialViewController: UIViewController = UIViewController()
    var onDeinit: (() -> Void)?
    
    convenience init(deallocationCallback: @escaping (() -> Void)) {
        self.init()
        self.onDeinit = deallocationCallback
    }
    

    func start() {
    }
    
    deinit {
        self.onDeinit?()
    }

}

